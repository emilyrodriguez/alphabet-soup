//import File System node module
const fs = require("fs");

//reads file synchronously, removes excess whitespaces and splits into array of lines
function readFile(fileName) {
	return fs.readFileSync(fileName, "utf-8").trim().split("\n");
}

//Takes file content and extracts the dimensions of the word search grid
function findDimensions(file) {
	//Splits first line by 'x', then converts the strings to numbers
	const [totalRows, totalCols] = file[0].split("x").map(Number);
	return { totalRows, totalCols };
}
/** uses slice to extract the lines corresponding to the grid, 
    removes carriage returns with replace(/\r/g, ''), 
    splits each row by space, and filters out empty characters. */

function createMatrix(file, totalRows) {
    // console.log('file:', file);
    // console.log('totalRows:', totalRows);

    //Returns a matrix created from file
	return file.slice(1, 1 + totalRows).map((row) =>
		row
			.replace(/\r/g, "")
			.split(" ")
			.filter((char) => char !== "")
	);
}

// Checks if a word exists in the matrix starting from a specificied position and moving in a specified direction
function searchWord(word, matrix, startRow, startCol, direction) {
	const wordLength = word.length;
	const [rowDelta, colDelta] = direction;

    //Iterating through each character
	for (let i = 0; i < wordLength; i++) {
		const currentRow = startRow + i * rowDelta;
		const currentCol = startCol + i * colDelta;
        //checks if the current position is inbounds or doesn't match the word
		if (
			currentRow < 0 ||
			currentRow >= matrix.length ||
			currentCol < 0 ||
			currentCol >= matrix[currentRow].length ||
			matrix[currentRow][currentCol] !== word[i]
		) {
			return false;
		}
	}
    //Return true if the word is found in the specified direction
	return true;
}

//Uses file, dimensions and maxtrix to find words in the grid
function findWords(file, dimensions, matrix) {
    const { totalRows, totalCols } = dimensions;

    // Iterates over each word in the file
    for (let i = totalRows + 1; i < file.length; i++) {
        const word = file[i].trim();

        // Iterates over each cell in the matrix
        for (let row = 0; row < totalRows; row++) {
            for (let col = 0; col < totalCols; col++) {
                // Checks if the first letter of the word matches the current matrix cell
                if (matrix[row][col] == word[0]) {
                    // Defines eight possible directions to search for the word
                    const directions = [
                        [1, 1],
                        [1, 0],
                        [1, -1],
                        [0, -1],
                        [-1, -1],
                        [-1, 0],
                        [-1, 1],
                        [0, 1],
                    ];

                    // Iterates over each direction to search for the word
                    for (const direction of directions) {
                        // Checks if the word is found in the current direction or its reverse
                        if (
                            searchWord(word, matrix, row, col, direction) ||
                            searchWord(word, matrix, row, col, [
                                -direction[0],
                                -direction[1],
                            ])
                        ) {
                            // Prints the word and its starting and ending indices in the desired format
                            console.log(
                                `${word} ${row}:${col} ${
                                    row + (word.length - 1) * direction[0]
                                }:${col + (word.length - 1) * direction[1]}`
                            );
                        }
                    }
                }
            }
        }
    }
}

//Executes word search solution
function findSolution() {
	const fileName = "sample1.txt";
	const file = readFile(fileName);
    const dimensions = findDimensions(file);
    const matrix = createMatrix(file, dimensions.totalRows);
    findWords(file, dimensions, matrix);
}

findSolution();